#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
#import re
import sys
import glob
import time
import threading
import subprocess
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import Qt
from PyQt4.QtWebKit import *
from fpdf import FPDF
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
from string import maketrans
#from multiprocessing.pool import ThreadPool
from multiprocessing import Pool
from urlparse import urlparse
#import skins
import logging

#if str(datetime.date.today()) <> '2016-04-24':281fbfee416c88af8f115935142f1ba1
#os.chdir(os.path.dirname(os.path.abspath(__file__)))
#if subprocess.call('C:/PDFConverter/2.exe', shell=True) == 1:#u'G:/2.exe'
    #sys.exit()

def LoadSettings_():
    import xml.etree.ElementTree as ET
    if not os.path.isfile('settings.xml'):
        app = QtGui.QApplication(sys.argv)
        QtGui.QMessageBox.information(None, u"Инфо",u"Файл settings.xml не найден",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
        sys.exit(app.exec_())

        sys.exit()

    tree = ET.parse('settings.xml')
    root = tree.getroot()
    settings = {}
    for child in root:
        settings[child.tag] = child.text
    return settings


class TabDialog(QtGui.QDialog):
    def __init__(self, fileName, parent=None):
        super(TabDialog, self).__init__(parent)

        fileInfo = QtCore.QFileInfo(fileName)

        tabWidget = QtGui.QTabWidget()
        tabWidget.setUsesScrollButtons(False)
        tabWidget.addTab(UrlToPDF(fileInfo), "Url to PDF")
        tabWidget.addTab(PDFToTxt(fileInfo), "PDF to Txt")
        tabWidget.addTab(TxtToPDF(fileInfo), "Txt to PDF")
        tabWidget.addTab(ImageToEBook(fileInfo), "Image to EBook")
        tabWidget.addTab(Settings(fileInfo), "Settings")

        buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)

        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(tabWidget)
        mainLayout.addWidget(buttonBox)
        self.setLayout(mainLayout)

        self.setWindowTitle("ConvertPDF")
        #self.setWindowOpacity(0.5)


class PDF(FPDF):
    def __init__(self, font, style, size, R, G, B, parent=None):
        FPDF.__init__(self)
    #вызов метода родительского класса + дописываем что-то свое
        self.font = font
        self.style = style
        self.size = size
        self.R = R
        self.G = G
        self.B = B

    def footer(self):
        self.set_y(-15)
        self.set_font('Arial', 'I', 12)
        self.set_text_color(128)
        self.cell(0, 10, str(self.page_no()), 0, 0, 'C')
 
    def chapter_body(self, name, encodingSourceFile='cp1251'):
        with open(name, 'rb') as fh:
            txt = fh.read().decode(encodingSourceFile)
 
        self.set_font(self.font, self.style, self.size)
        self.set_text_color(self.R, self.G, self.B)

        self.multi_cell(0, 5, txt)
        self.ln()
 
    def print_chapter(self, num, title, name, encodingSourceFile, orientation):
        self.add_page(orientation)
        self.chapter_body(name, encodingSourceFile)


class QSelectPath(QtGui.QWidget):
    """
    Виджет для выбора пути к файлу
    """
    def __init__(self, file_or_folder, parent=None):
        super(QSelectPath, self).__init__(parent)
        self.path = None
        self.file_or_folder = file_or_folder
        self.build_widget()
 
    def build_widget(self):
        main_layout = QtGui.QHBoxLayout()
 
        # Убираем внешние отступы у компановщика
        main_layout.setContentsMargins(0, 0, 0, 0)
 
        self.path = QtGui.QLineEdit(self)
        self.path.setReadOnly(True)
        self.path.setFocusPolicy(QtCore.Qt.NoFocus)
 
        overview = QtGui.QPushButton(u'Обзор...', self)
        overview.setFocusPolicy(QtCore.Qt.NoFocus)
        self.connect(overview, QtCore.SIGNAL('clicked()'), self.choose_file)
 
        main_layout.addWidget(self.path)
        main_layout.addWidget(overview)
 
        self.setLayout(main_layout)
 
    def choose_file(self):
        """
        Открывает диалоговое окно выбора папки
        """
        if self.file_or_folder == 'file':
            path = QtGui.QFileDialog.getOpenFileName(self, u'Выберите файл')
        else:
            path = QtGui.QFileDialog.getExistingDirectory(self, u'Выберите папку')

        if path:
            self.path.setText(path)
 
    def text(self):
        return unicode(self.path.text())
 
    def set_text(self, string):
        self.path.setText(string)

#def browse():
#    self.path.setText(QtGui.QFileDialog.getExistingDirectory(self, u"Выбор каталога с изображениями",QtCore.QDir.currentPath()))

def overWrite(var):
    app = QtGui.QApplication(sys.argv)
    reply = QtGui.QMessageBox.question(None, 'Message', var, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
    if reply == QtGui.QMessageBox.Yes:
        return True
    else:
        return False
    sys.exit(app.exec_())

class UrlToPDF(QtGui.QWidget):
    def __init__(self, fileInfo, parent=None):
        super(UrlToPDF, self).__init__(parent)

        url = QtGui.QLineEdit('', self)
        #OutputFolder = QtGui.QLineEdit('', self)
        self.path = QSelectPath('folder')
        
        #browseButton = QtGui.QPushButton(u'Обзор...', self)
        #browseButton.clicked.connect(browse)

        self.SaveButton = QtGui.QPushButton(u'Сохранить', self)
        self.connect(self.SaveButton, QtCore.SIGNAL('clicked()'), lambda : self.SavePDF(url.text()))#в следующий раз попробовать unicode(url.text())

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(QtGui.QLabel("URL:"))
        mainLayout.addWidget(url)
        mainLayout.addWidget(QtGui.QLabel(u"Выходная папка:"))
        #mainLayout.addWidget(OutputFolder)
        mainLayout.addWidget(self.path)

        #self.connect(overview, QtCore.SIGNAL('clicked()'), self.choose_file)
 
        #main_layout.addWidget(self.path)
        #mainLayout.addWidget(browseButton)
        
        mainLayout.addWidget(self.SaveButton)

        mainLayout.addStretch(1)
        self.setLayout(mainLayout)

    def SavePDF(self, url):

        for c in unicode(url):
            if c in u'абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ':
                #print 'cyrillic detected'
                urlCyr = urlparse(unicode(url))
                url = urlCyr.scheme+'://'+urlCyr.netloc.encode("idna")+urlCyr.path
                break
        #print unicode(url)

        def convertIt():
          web.print_(printer)
          #print("Pdf generated")
          QtGui.QMessageBox.information(self, u"Инфо",u"Страница "+unicode(url)+u" сохранена в файл "+unicode(self.path.text())+'/'+str(unicode(url)).translate(maketrans('/\:*?"<>|', '_________'))+".pdf",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
          self.SaveButton.setText(u"Сохранить")
          self.SaveButton.setChecked(False)
        '''pattern = re.compile(
            r'^(?:http|ftp)s?://' # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
            r'localhost|' #localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
            r'(?::\d+)?' # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if pattern.match(url) is None:
            QtGui.QMessageBox.information(self, u"Инфо",u"Неверный URL",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return'''
        if len(self.path.text()) < 3:
            QtGui.QMessageBox.information(self, u"Инфо",u"Неверный путь к выходной папке",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        
        self.SaveButton.setText(u"Сохранение...")
        self.SaveButton.setCheckable(True)
        self.SaveButton.setChecked(True)
        web = QWebView()
        web.load(QtCore.QUrl(url))
        #web.show() можно показать страницу
        printer = QtGui.QPrinter()
        printer.setPageSize(QtGui.QPrinter.A4)
        printer.setOutputFormat(QtGui.QPrinter.PdfFormat)

        if len(self.path.text() + '/' + str(url).translate(maketrans('/\:*?"<>|', '_________')) + '.pdf') > 246:
            url = int(time.time())
            self.path.set_text(self.path.text()[:3])
            p = MyThreadInfo(u"Слишком длинный URL или путь. Попытка сохранить в " + unicode(self.path.text()) + unicode(str(url)) + ".pdf")
            p.start()
            #th.join() - вызвает ошибки, выяснить нужно ли завершать поток
            #или так
            #p1 = threading.Thread(target=infoBox, name="t1", args=[u"Слишком длинный URL или путь. Попытка сохранить в " + unicode(self.path.text()) + unicode(str(url)) + ".pdf"])
            #p1.start()
            # наличие именно этой инструкции
            #QtGui.QMessageBox.information(self, u"Инфо",u"Слишком длинный URL или путь. Попытка сохранить в " + unicode(self.path.text()) + unicode(str(url)) + ".pdf",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
        if os.path.isfile(self.path.text() + '/' + str(url).translate(maketrans('/\:*?"<>|', '_________')) + '.pdf'):
            #QtGui.QMessageBox.information(self, u"Инфо",u"Файл " + self.path.text() + '/' + str(url).translate(maketrans('/\:*?"<>|', '_________')) + '.pdf' + u' уже существует',buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            #reply = QtGui.QMessageBox.question(self, 'Message', u"Файл " + self.path.text() + '/' + str(url).translate(maketrans('/\:*?"<>|', '_________')) + '.pdf' + u' уже существует. Перезаписать?', QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
            #p1 = threading.Thread(target=overWrite, name="t1", args=[u"Вопрос"])
            #p1.start()
            p = Pool(1)
            if p.map(overWrite, [u"Файл " + self.path.text() + '/' + str(url).translate(maketrans('/\:*?"<>|', '_________')) + '.pdf' + u' уже существует. Перезаписать?']) == [False]:
                self.SaveButton.setText(u"Сохранить")
                self.SaveButton.setChecked(False)
                return

        printer.setOutputFileName(self.path.text() + '/' + str(url).translate(maketrans('/\:*?"<>|', '_________')) + '.pdf')#table = maketrans('/\:*?"<>|', '_________')

        self.connect(web, QtCore.SIGNAL("loadFinished(bool)"), convertIt)


class MyThreadInfo(threading.Thread):
    def __init__(self, a):
        threading.Thread.__init__(self)
        self.a = a

    def run(self):
        window = QtGui.QWidget()
        window.setWindowTitle(u"Класс QMessageBox")
        window.resize(300, 70)
        QtGui.QMessageBox.information(None, u"Инфо",self.a,buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)


def decorator(F):       #F - функция или метод, не связанный с экземпляром
    def wrapper(*args): # для методов - экземпляр класса в args[0]
        # F(*args) - вызов функции или метода
        my_thread = threading.Thread(target=F, args=args)
        my_thread.start()
    return wrapper

'''@decorator
def func(x, y):
    print x, y
func(6, 7)'''
class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level

    def write(self, buf):
        print type(buf)
        #for line in buf.rstrip().splitlines():
        self.logger.log(self.log_level, buf)

class PDFToTxt(QtGui.QWidget):
    #1) mysignal = QtCore.pyqtSignal(bool)
    def __init__(self, fileInfo, parent=None):
        super(PDFToTxt, self).__init__(parent)

        self.PDFFilePath = QSelectPath('file')
        #OutputFolder = QtGui.QLineEdit('', self)
        self.OutputFolder = QSelectPath('folder')
        
        #browseButton = QtGui.QPushButton(u'Обзор...', self)
        #browseButton.clicked.connect(browse)

        self.ConvertPDFToTxtButton = QtGui.QPushButton(u'Конвертировать', self)
        self.connect(self.ConvertPDFToTxtButton, QtCore.SIGNAL('clicked()'), self.Convert)

        #2) self.mysignal[bool].connect(lambda : self.ConvertPDFToTxtButton.setDisabled(True), Qt.QueuedConnection)#нужно ли Qt.QueuedConnection?

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(QtGui.QLabel(u"Выбрать файл:"))
        mainLayout.addWidget(self.PDFFilePath)
        mainLayout.addWidget(QtGui.QLabel(u"Выходная папка:"))
        #mainLayout.addWidget(OutputFolder)
        mainLayout.addWidget(self.OutputFolder)

        #self.connect(overview, QtCore.SIGNAL('clicked()'), self.choose_file)
 
        #main_layout.addWidget(self.path)
        #mainLayout.addWidget(browseButton)
        
        mainLayout.addWidget(self.ConvertPDFToTxtButton)

        mainLayout.addStretch(1)
        self.setLayout(mainLayout)

    #декоратор
    '''def thread(my_func):
        def wrapper(*args, **kwargs):
            my_thread = threading.Thread(target=my_func, args=args, kwargs=kwargs)
            my_thread.start()
        return wrapper'''

    @decorator
    def Convert(self):
        if len(self.PDFFilePath.text()) < 6:
            QtGui.QMessageBox.information(self, u"Инфо",u"Неверное имя файла",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        if len(self.OutputFolder.text()) < 3:
            QtGui.QMessageBox.information(self, u"Инфо",u"Неверный путь к выходной папке",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        OutTxtFile = os.path.basename(self.PDFFilePath.text())[:-3]+'txt'
        if os.path.isfile(unicode(self.OutputFolder.text()+"/"+OutTxtFile)):
            #QtGui.QMessageBox.information(self, u"Инфо",u"Файл " + self.OutputFolder.text()+"/"+OutTxtFile + u' уже существует. Перезаписать?',buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            p = Pool(1)
            if p.map(overWrite, [u"Файл " + self.OutputFolder.text()+"/"+OutTxtFile + u' уже существует. Перезаписать?']) == [False]:
                return
        #не работает
        #p = threading.Thread(target=self.ThreadSetDisabled, name="t1")
        #p.start()
        #self.mysignal[bool].emit(True)
        self.ConvertPDFToTxtButton.setDisabled(True)
        #print type(OutTxtFile)
        #return

        cmd = u"pdftotext.exe -layout -enc UTF-8 \""+self.PDFFilePath.text()+"\" \""+self.OutputFolder.text()+"/"+OutTxtFile+"\"".encode('cp1251')
        #print ' Value OutTxtFile: ' + OutTxtFile
        #subprocess.Popen(cmd.encode('cp1251'), shell = True)

        #Получение всего результата после завершения программы
        PIPE = subprocess.PIPE
        p = subprocess.Popen(cmd.encode('cp1251'), shell=True, stdin=PIPE, stdout=PIPE, stderr=subprocess.STDOUT)#open("log.txt", "a")
        stdout = p.stdout.read()
        #если произошла ошибка при конвертации пишем ее в лог
        if stdout != '':
            logging.basicConfig(format='\n%(asctime)s:%(levelname)s:%(name)s:%(message)s',filename="log.log")
            stderr_logger = logging.getLogger('PDFToTxt')
            stderr_logger.error('\n'+stdout)
            #logging.exception('')
            QtGui.QMessageBox.information(self, u"Инфо",u"Произошла ошибка при конвертации. Информация будет добавлена в журнал ошибок",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)

        self.ConvertPDFToTxtButton.setDisabled(False)
        
        if os.path.isfile(unicode(self.OutputFolder.text()+"/"+OutTxtFile)):
            QtGui.QMessageBox.information(
                self, u'Информация', u"Файл конвертирован в "+self.OutputFolder.text()+"/"+OutTxtFile,
                QtGui.QMessageBox.Close
            )
        else:
            print u'Ошибка конвертации'


class TxtToPDF(QtGui.QWidget):
    s_convert_finish = QtCore.pyqtSignal(bool, name='convert_finish')

    def __init__(self, fileInfo, parent=None):
        super(TxtToPDF, self).__init__(parent)

        self.comboBox = QtGui.QComboBox()
        Items = ['8', '9', '10', '11', '12', '14' ,'16', '18', '20', '22', '24', '26', '28', '36', '48', '72']
        #self.comboBox.addItem("8", 8)
        #self.comboBox.addItem("9", 9)
        self.comboBox.addItems(Items)
        self.comboBox.setCurrentIndex(4)
        #self.comboBox = QtGui.QFontComboBox()
        #self.comboBox.setCurrentIndex(202)
        self.ColorButton = QtGui.QPushButton(u'Цвет текста')
        self.QLineColor = QtGui.QLineEdit('#000000')
        self.TxtFilePath = QSelectPath('file')
        self.comboBox2 = QtGui.QComboBox()
        Items2 = ['cp1251', 'utf8']
        self.comboBox2.addItems(Items2)
        orientation = QtGui.QGroupBox(u'Ориентация')
        self.ConvertTxtToPDFButton = QtGui.QPushButton(u'Конвертировать')

        self.ColorButton.clicked.connect(self.showColorDialog)
        self.connect(self.ConvertTxtToPDFButton, QtCore.SIGNAL('clicked()'), self.start_convert)

        self.radio1 = QtGui.QRadioButton(u"Книжная")
        self.radio1.setChecked(True)
        self.radio2 = QtGui.QRadioButton(u"Альбомная")
        #buttonGroup = QtGui.QButtonGroup()
        #buttonGroup.addButton(radio1, 1)
        #buttonGroup.addButton(radio2, 2) с. 494

        RadioButtonBox = QtGui.QVBoxLayout()
        #RadioButtonBox.addWidget(self.comboBox2)
        RadioButtonBox.addWidget(self.radio1)
        RadioButtonBox.addWidget(self.radio2)

        orientation.setLayout(RadioButtonBox)

        #radio_encodingLayout = QtGui.QHBoxLayout()
        #radio_encodingLayout.addWidget(orientation)

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(QtGui.QLabel(u"Размер:"))
        mainLayout.addWidget(self.comboBox) 
        mainLayout.addWidget(self.ColorButton)
        mainLayout.addWidget(self.QLineColor)
        mainLayout.addWidget(QtGui.QLabel(u'Выбрать файл:'))
        mainLayout.addWidget(self.TxtFilePath)
        mainLayout.addWidget(QtGui.QLabel(u"Кодировка исходного файла:"))
        mainLayout.addWidget(self.comboBox2)
        mainLayout.addWidget(orientation)
        #mainLayout.addWidget(radio_encodingLayout)
        mainLayout.addWidget(self.ConvertTxtToPDFButton)

        mainLayout.addStretch(1)
        self.setLayout(mainLayout)

        self.s_convert_finish.connect(self.finish_convert, Qt.QueuedConnection)

    def showColorDialog(self):
        color = QtGui.QColorDialog.getColor()#color.name()[1:3]
        self.QLineColor.setText(color.name())
        #v =int(unicode(self.QLineColor.text()[5:7]), 16)
        #QtGui.QMessageBox.information(self, u"Инфо",u"Переменная = "+str(v),buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)

    #Запускает функцию в отдельном потоке
    def thread(my_func):
        def wrapper(*args, **kwargs):
            my_thread = threading.Thread(target=my_func, args=args, kwargs=kwargs)
            my_thread.start()
        return wrapper

    @thread
    def convert(self, path, signal):
        try:
            #int('str', 16) - перевод числа из HEX с учетом знака в DEC
            pdf = PDF('DejaVu', '', int(self.comboBox.currentText()), int(unicode(self.QLineColor.text()[1:3]), 16), int(unicode(self.QLineColor.text()[3:5]), 16), int(unicode(self.QLineColor.text()[5:7]), 16))
            pdf.add_font('DejaVu', '', 'DejaVuSansCondensed.ttf', uni=True)#'Arial', '', 'C:/WINDOWS/Fonts/Arial.ttf',
            #pdf.add_font('Arial', '', 'C:/WINDOWS/Fonts/Arial.ttf', uni=True)
            pdf.print_chapter(2, 'THE PROS AND CONS', path, str(self.comboBox2.currentText()), 'P' if self.radio1.isChecked() == True else 'L')
            pdf.output(path[:-3] + 'pdf', 'F')
        except Exception:
            logging.basicConfig(format='\n%(asctime)s %(levelname)s:%(message)s',filename="log.log")
            logging.exception('')
            QtGui.QMessageBox.information(self, u"Инфо",u"Произошла ошибка при конвертации. Информация будет добавлена в журнал ошибок",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            self.ConvertTxtToPDFButton.setDisabled(False)
            sys.exit()
        signal.emit(True)

    def start_convert(self):
        if len(self.QLineColor.text()) <> 7:
            QtGui.QMessageBox.information(self, u"Инфо",u"Неверное значение цвета",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        if len(self.TxtFilePath.text()) < 6:
            QtGui.QMessageBox.information(self, u"Инфо",u"Неверное имя файла",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        if os.path.isfile(self.TxtFilePath.text()[:-3] + 'pdf'):
            #QtGui.QMessageBox.information(self, u"Инфо",u"Файл " + self.TxtFilePath.text()[:-3] + 'pdf' + u' уже существует',buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            p = Pool(1)
            if p.map(overWrite, [u"Файл " + self.TxtFilePath.text()[:-3] + 'pdf' + u' уже существует. Перезаписать?']) == [False]:
                return
        self.ConvertTxtToPDFButton.setDisabled(True)
        #path = self.all_tabs[0].layout().itemAt(1).widget().text() AttributeError: 'PermissionsTab' object has no attribute 'all_tabs'
        #QtGui.QMessageBox.information(self, u"Инфо",u"Переменная = "+self.comboBox.currentFont().family(),buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
        self.convert(self.TxtFilePath.text(), self.s_convert_finish)
 
    def finish_convert(self, event):
        self.ConvertTxtToPDFButton.setDisabled(False)
        QtGui.QMessageBox.information(
            self, u'Информация', u"Файл конвертирован в "+self.TxtFilePath.text()[:-3]+u"pdf",
            QtGui.QMessageBox.Close
        )


class ImageToEBook(QtGui.QWidget):
    def __init__(self, fileInfo, parent=None):
        super(ImageToEBook, self).__init__(parent)

        topLabel = QtGui.QLabel(u"Каталог с изображениями:")
        self.ImagesPath = QSelectPath('folder')
        self.OutputFolder = QSelectPath('folder')
        #self.path = QtGui.QLineEdit(self)
        #browseButton = QtGui.QPushButton(u"Обзор...", self)
        #browseButton.clicked.connect(self.browse)
        self.ConvertImageToPDFButton = QtGui.QPushButton(u'Конвертировать')
        self.ConvertImageToPDFButton.clicked.connect(self.convertLogging)
        self.pbar = QtGui.QProgressBar(self)
        self.pbar.setGeometry(30, 40, 200, 25)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(topLabel)
        layout.addWidget(self.ImagesPath)
        layout.addWidget(QtGui.QLabel(u"Выходная папка:"))
        layout.addWidget(self.OutputFolder)
        #layout.addWidget(browseButton)
        layout.addWidget(self.ConvertImageToPDFButton)
        layout.addWidget(self.pbar)
        layout.addStretch(1)
        self.setLayout(layout)
    #def browse(self):
    #    self.path.setText(QtGui.QFileDialog.getExistingDirectory(self, u"Выбор каталога с изображениями",QtCore.QDir.currentPath()))
    def convertLogging(self):
        try:
            self.convert()
        except Exception:
            logging.basicConfig(format='\n%(asctime)s %(levelname)s:%(message)s',filename="log.log")
            logging.exception('')
            QtGui.QMessageBox.information(self, u"Инфо",u"Произошла ошибка при конвертации. Информация будет добавлена в журнал ошибок",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            #sys.exit()
    def convert(self):
        if len(self.ImagesPath.text()) < 3:
            QtGui.QMessageBox.information(self, u"Инфо",u"Неверный путь к каталогу с изображениями",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        if len(self.OutputFolder.text()) < 3:
            QtGui.QMessageBox.information(self, u"Инфо",u"Неверный путь к выходной папке",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return
        if os.path.isfile(self.OutputFolder.text() + '/ebook.pdf'):
            #QtGui.QMessageBox.information(self, u"Инфо",u"Файл " + self.OutputFolder.text() + '/ebook.pdf' + u' уже существует',buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            p = Pool(1)
            if p.map(overWrite, [u"Файл " + self.OutputFolder.text() + '/ebook.pdf' + u' уже существует. Перезаписать?']) == [False]:
                return

        c = canvas.Canvas(self.OutputFolder.text()+'/ebook.pdf')
        pictures_jpg = glob.glob(unicode(self.ImagesPath.text()) + "\\%s*" % '*.jpg')
        pictures_gif = glob.glob(unicode(self.ImagesPath.text()) + "\\%s*" % '*.gif')
        pictures_png = glob.glob(unicode(self.ImagesPath.text()) + "\\%s*" % '*.png')
        pictures = pictures_jpg + pictures_gif + pictures_png
        if len(pictures) == 0:
            QtGui.QMessageBox.information(self, u"Инфо",u"Изображений не найдено",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return

        i = 1
        for pic in pictures:
            c.drawImage(pic, 0, 0, 21*cm, 29.7*cm)
            c.showPage()
            #print (u'Сохранение %d%%' % (i*100/len(pictures)))
            self.pbar.setValue(i*100/len(pictures))
            i += 1

        c.save()


class Settings(QtGui.QWidget):
    def __init__(self, fileInfo, parent=None):
        super(Settings, self).__init__(parent)

        skinsList = ['WindowsXP', 'Windows', 'Plastique', 'Cde', 'Motif', 'Sgi' ,'Cleanlooks', 'Mac', 'QDarkStyle', 'Yellow', 'Light-blue', 'Light-green', 'Light-orange', 'Dark-blue', 'Dark-green', 'Dark-orange']
        #print skinsList.index()
        LoadSettings = self.LoadSettings()

        if skinsList.index(LoadSettings['Skin']) < 8:
            app.setStyleSheet('')
            app.setStyle(LoadSettings['Skin'])
        else:
            app.setStyleSheet(open("skins/"+LoadSettings['Skin']+".qss","r").read())

        #app.setStyle(LoadSettings['Skin'])
        self.comboBox = QtGui.QComboBox()
        self.SaveButton = QtGui.QPushButton(u'Сохранить', self)
        self.connect(self.SaveButton, QtCore.SIGNAL('clicked()'), self.SaveSettings)
        #Items = ['WindowsXP', 'Windows', 'Plastique', 'Cde', 'Motif', 'Sgi' ,'Cleanlooks', 'Mac', 'Light-blue', 'Light-green', 'Light-orange', 'Dark-blue', 'Dark-green', 'Dark-orange']
        #self.comboBox.addItem("8", 8)
        #self.comboBox.addItem("9", 9)
        self.comboBox.addItems(skinsList)
        self.comboBox.setCurrentIndex(skinsList.index(LoadSettings['Skin']))
        self.comboBox.currentIndexChanged.connect(self.selectionchange)

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.comboBox)
        layout.addWidget(self.SaveButton)

        self.setLayout(layout)
    def SaveSettings(self):
            # способ 2 (лучший) - модификация существующего xml-файла средствами библиотеки ElementTree
            import hashlib
            import xml.etree.ElementTree as ET
            sha512OldFileHash = hashlib.sha512(open('settings.xml').read()).hexdigest()
            tree = ET.parse('settings.xml')

            root = tree.getroot()
            root[0].text = str(self.comboBox.currentText())
            tree.write('settings.xml', xml_declaration=True, encoding='utf-8', method="xml")
            #Проверить сохранен ли файл наверное можно с помощью сравнения хэшей TTH, SHA1, MD5, до и после модификации файла?
            if sha512OldFileHash != hashlib.sha512(open('settings.xml').read()).hexdigest():
                self.SaveButton.setDisabled(True)

    def LoadSettings(self):
        import xml.etree.ElementTree as ET
        if not os.path.isfile('settings.xml'):
            app = QtGui.QApplication(sys.argv)
            QtGui.QMessageBox.information(None, u"Инфо",u"Файл settings.xml не найден",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            sys.exit(app.exec_())
            #print(u'Файл settings.xml не найден')
            sys.exit()
        tree = ET.parse('settings.xml')
        root = tree.getroot()
        settings = {}
        for child in root:
            settings[child.tag] = child.text
        return settings

    def selectionchange(self, i):
        self.SaveButton.setDisabled(False)
        if i < 8:
            app.setStyleSheet('')
            app.setStyle(self.comboBox.currentText())
        else:
            app.setStyleSheet(open("skins/"+self.comboBox.currentText()+".qss","r").read())


if __name__ == '__main__':

    import sys

    app = QtGui.QApplication(sys.argv)
    # setup stylesheet
    #app.setStyleSheet("QPlainTextEdit,QSpinBox,QTimeEdit,QLineEdit { border-radius: 5;}")
    #app.setStyle("plastique")
    #app.setStyleSheet("QLineEdit { background-color: yellow }")
    #app.setStyleSheet("QPushButton { background-color: lightblue; border: none; }")
    #app.setStyleSheet(skins.s3)
    #app.setStyleSheet(open("./QDarkStyle.qss","r").read())

    if len(sys.argv) >= 2:
        fileName = sys.argv[1]
    else:
        fileName = "."

    tabdialog = TabDialog(fileName)
    sys.exit(tabdialog.exec_())
