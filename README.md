
"PDF Converter" is a desktop application for working with pdf files.
The application is written in Python 2.7 and GUI in PyQt4.

Program features.

1. Saving website from url to pdf-file
2. Convert pdf file to text
3. Convert text files to pdf-files with the ability to change the color of the text, encoding, and page orientation
4. Creation from several images of one pdf-file(ebook)